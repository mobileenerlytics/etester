Instructions to setup Eagle Tester source code are WIP.

In the meantime, you can checkout: 

* The demo video: https://www.youtube.com/watch?v=5SCcIYdMGmE
* Instructions to setup Eagle Tester: https://beta.mobileenerlytics.com/instructions.html

Following instructions are WIP. Please come back later.
## Deploying the server
Pull the source code and **build**.
```sh
git clone git@bitbucket.org:mobileenerlytics/open-server.git
cd open-server
export ORG_GRADLE_PROJECT_deployerpwd=
export ORG_GRADLE_PROJECT_dockerpwd=
./gradlew build
# This may take a while. Wait for it to finish.
```

**Running the server**: First, make sure that you are running a mongo server locally on its default port 27017. [See this for instructions on running mongo locally](https://docs.mongodb.com/manual/administration/install-community/).
```sh
cd eagle-tester-server/build/distributions/
tar -xvf eagle-tester-server-*-SNAPSHOT.tar
cd eagle-tester-server-*
cd bin
./eagle-tester-server
```

The server is now available at http://localhost:38673. Verify by visiting this URL with your browser and check server logs for any crashes.

**Configuration**: The server is highly configurable. Create a file named `application.properties` such as following.
```sh
# DB properties. If you want to use remote database.
uri=mongodb+srv://user:password@yourcluster.mongodb.net/database_name
database=database_name

# Storage properties. If you want to store raw logs in AWS S3 instead of on local filesystem.
aws.access_key_id=
aws.secret_access_key=
aws.s3.bucket_name=

## Email properties. If you want to send emails from server.
mail.smtp.host=smtp.gmail.com
mail.smtp.port=587
mail.smtp.auth=true
mail.smtp.starttls.enable=true
mail.smtp.username=
mail.smtp.password=

## Server properties. Configure port and base URL of server.
server.baseUrl=https://your.URL.com
server.port=
```

Keep only the properties you want to configure in your `application.properties`. To run the server with these configurations:
```sh
cd eagle-tester-server/build/distributions/eagle-tester-server-*
cd bin
./eagle-tester-server --spring.config.location=<path_to_application.properties>
```